import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  images: string[] = [
    'assets/img/baner1.jpg',
    'assets/img/baner2.jpg',
    'assets/img/baner3.jpg',
    'assets/img/baner5.jpg'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
