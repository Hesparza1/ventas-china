import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './components/products/products.component';
import { ProductDComponent } from './components/productd/productd.component';
import { ProductComponent } from './components/product/product.component';
import { ProductRouting } from './product.routing.module';



@NgModule({
  declarations: [
    ProductsComponent,
    ProductDComponent,
    ProductComponent
  ],
  imports: [
    CommonModule,
    ProductRouting
  ]
})
export class ProductModule { }
