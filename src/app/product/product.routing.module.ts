import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { ProductDComponent } from './components/productd/productd.component';

const routes: Routes = [
    {
        path: '',
        component: ProductsComponent
     }
    // {
    //     path: '',
    //     component: ProductDComponent
    // }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ]
})

export class ProductRouting {}
