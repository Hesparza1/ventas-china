import { Injectable } from '@angular/core';
import { Product } from '../product/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: Product[] = [
    {
      id: '1',
      image: 'assets/img/reloj.jpeg',
      title: 'reloj',
      price: 270,
      description: 'Reloj metalico tipo casio'
    },
    {
      id: '2',
      image: 'assets/img/pinkdd.jpeg',
      title: 'Bolsa Pink D',
      price: 350,
      description: 'Bolsa deportiva rosa'
    },
    {
      id: '3',
      image: 'assets/img/pink.jpg',
      title: 'Bolsa Pink C',
      price: 270,
      description: 'Bolsa clasica con lentejuela'
    },
    {
      id: '4',
      image: 'assets/img/espejo.jpeg',
      title: 'Espejo led',
      price: 150,
      description: 'Espejo de colores con luz led'
    },
    {
      id: '5',
      image: 'assets/img/plancha.jpeg',
      title: 'Mini plancha',
      price: 270,
      description: 'Mini plancha para el cabello de colores'
    }
  ];
  constructor() { }

  getAllp(){
    return this.products;
  }

  getproduct(id: string){
    return this.products.find(item => id === item.id);
  }
}
