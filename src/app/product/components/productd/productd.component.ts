import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from '../products/products.service';
import { Product } from '../product/product.model';

@Component({
  selector: 'app-productd',
  templateUrl: './productd.component.html',
  styleUrls: ['./productd.component.scss']
})
export class ProductDComponent implements OnInit {

  product: Product;
  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService

    ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.product = this.productsService.getproduct(id);
    });
  }

}
