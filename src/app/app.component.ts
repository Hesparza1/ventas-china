import { Component } from '@angular/core';
import { Product } from './product/components/product/product.model';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.css']
})

export class AppComponent {}
